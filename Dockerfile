FROM python:3-alpine
LABEL maintainer="Erwan Rouchet <lucidiot@protonmail.com>"

ADD . /src
RUN cd /src && pip install . && cd / && rm -rf src

ENTRYPOINT ["python", "-m", "wattsim_simulator"]
CMD ["--url", "http://wattsim-backend/"]
