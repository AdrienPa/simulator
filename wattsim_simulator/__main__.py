#!/usr/bin/env python3
from getpass import getpass
from wattsim_simulator import StationSimulator
import argparse
import logging
import os


def main():
    parser = argparse.ArgumentParser(
        prog='wattsim_simulator',
        description='WattSim station simulator',
    )
    parser.add_argument(
        '--url',
        default=os.environ.get('WATTSIM_URL', 'http://localhost:8000/'),
        help='Backend URL. Can be set using the WATTSIM_URL environment variable.',
    )
    parser.add_argument(
        '-u', '--username',
        help='Username to use to authenticate to the backend API. '
             'If left unspecified, will be asked interactively. '
             'Can also be set using the WATTSIM_USERNAME environment variable.',
    )
    parser.add_argument(
        '-p', '--password',
        help='Password to use to authenticate to the backend API. '
             'If left unspecified, will be asked interactively. '
             'Can also be set using the WATTSIM_PASSWORD environment variable.',
    )
    parser.add_argument(
        '-f', '--frequency',
        help='Frequency, in seconds, of the measurement updates. '
             'Can also be set using the WATTSIM_FREQUENCY environment variable.',
        default=os.environ.get('WATTSIM_FREQUENCY', '10.0'),
        type=float,
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_const',
        const=logging.DEBUG,
        default=logging.INFO,
        dest='loglevel',
        help='Increase the logs verbosity. Useful for debugging.',
    )

    interactive_group = parser.add_mutually_exclusive_group()
    interactive_group.add_argument(
        '-i', '--interactive',
        help='Ask for the username and the password interactively. This is the default.',
        action='store_true',
        default=True,
        dest='interactive',
    )
    interactive_group.add_argument(
        '-I', '--non-interactive',
        help='Never ask for anything interactively. '
             'Can cause the script to crash with an exit status of 2 '
             'if a required argument is missing.',
        action='store_false',
        dest='interactive',
    )

    kwargs = vars(parser.parse_args())
    if not kwargs.get('username'):
        if 'WATTSIM_USERNAME' in os.environ:
            kwargs['username'] = os.environ['WATTSIM_USERNAME']
        elif kwargs['interactive']:
            kwargs['username'] = input('Username: ')
        else:
            parser.error('Missing WattSim username')

    if not kwargs.get('password'):
        if 'WATTSIM_PASSWORD' in os.environ:
            kwargs['password'] = os.environ['WATTSIM_PASSWORD']
        elif kwargs['interactive']:
            kwargs['password'] = getpass('Password: ')
        else:
            parser.error('Missing WattSim password')

    logging.basicConfig(
        format='[%(levelname)s/%(name)s] %(message)s',
        level=kwargs.pop('loglevel', logging.INFO),
    )
    del kwargs['interactive']

    StationSimulator(**kwargs).run()


if __name__ == '__main__':
    main()
