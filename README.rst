WattSim station simulator
=========================

This repository provides a simple station simulator that sends
random fake data to a WattSim backend for testing purposes.

Usage
-----

::

   pip install -r requirements.txt
   python3 -m wattsim_simulator \
        [--url http://...] \
        [-u/--username username] \
        [-p/--password password] \
        [-f/--frequency frequency] \
        [-i/--interactive | -I/--non-interactive] \
        [-v/--verbose]


``username``
   The username to use to authenticate to the backend API.
   If left unspecified, will be asked interactively, unless when using ``-I``.
   Can also be set using the ``WATTSIM_USERNAME`` environment variable.
``password``
   The password to use to authenticate to the backend API.
   You should only use this argument is the password is exclusively
   for testing purposes like ``p4ssw0rd`` and is not an actually
   important password!
   If left unspecified, will be asked interactively, unless when using ``-I``.
   Can also be set with the ``WATTSIM_PASSWORD`` environment variable.
``url``
   URL that points to the backend API.
   Defaults to ``http://localhost:8000/``, the backend's
   development server's default location.
   Can also be set with the ``WATTSIM_URL`` environment variable.
``frequency``
   How often, in seconds, to send fake data to the backend.
   Defaults to one measurement every 10 seconds.
   Can also be set with the ``WATTSIM_FREQUENCY`` environment variable.
``(non-)interactive``
   Toggles interactive mode; when the username and/or password are missing
   from the command line and the environment variables, will ask interactively
   for them. When disabled, will exit with a status code of 2 if any of those
   arguments are missing.
``verbose``
   Increase the logging verbosity, for debugging purposes.

Docker usage
------------

This repository provides a Docker image which you can use to run the
simulator without installing any dependencies::

   docker run -d registry.gitlab.com/wattsim/simulator:latest [arguments]
